import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs/Observable";

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = "https://reqres.in/";
  constructor(private httpClient: HttpClient) { }

  getUsuario(usuario_id): Observable<any>{
    return this.httpClient.get(this.url+'api/users/'+usuario_id)
  }
  getUsuarios(): Observable<any>{
    return this.httpClient.get(this.url+'api/unknow/')
  }
  addUsuario(user): Observable<any>{
    let params = JSON.stringify(user);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.httpClient.post(this.url + 'api/users', params, { headers: headers });
  }
}
