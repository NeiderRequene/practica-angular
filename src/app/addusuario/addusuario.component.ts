import { Component, OnInit } from '@angular/core';
import { Usuario } from "../models/usuario";
import { HttpClient } from "@angular/common/http";
import { UsuarioService } from '../services/usuario.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-addusuario',
  templateUrl: './addusuario.component.html',
  styleUrls: ['./addusuario.component.scss']
})
export class AddusuarioComponent implements OnInit {
  usuario: Usuario;
  constructor(private usuarioService: UsuarioService) {
    this.usuario = new Usuario('', '');
  }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    this.usuarioService.addUsuario(this.usuario).subscribe((resp) => {
      console.log(resp);
      form.reset();
    }, error => {
      console.log(error);

    })

  }
}
